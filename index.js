import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';
import cors from 'cors';
import { apolloUploadExpress } from 'apollo-upload-server';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { execute, subscribe } from 'graphql';

import models from './models';
import refreshTokens from './utils/refreshTokens';

dotenv.config();

const { SECRET, SECRET2 } = process.env;

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, './schema')));
const resolvers = mergeResolvers(fileLoader(path.join(__dirname, './resolvers')));

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const graphqlEndpoint = '/graphql';

const app = express();

app.use(cors('*'));

const addRestaurant = async (req, res, next) => {
  const authorization = req.headers['resqr-token'];
  const token = authorization ? authorization.replace('Bearer ', '') : undefined;
  if (token) {
    try {
      const { restaurant } = jwt.verify(token, SECRET);
      req.restaurant = restaurant;
    } catch (err) {
      const refreshAuth = req.headers['resqr-refresh-token'];
      const refreshToken = refreshAuth.replace('Bearer ', '');
      const newTokens = await refreshTokens(token, refreshToken, models, SECRET, SECRET2);
      if (newTokens.token && newTokens.refreshToken) {
        res.set('Access-Control-Expose-Headers', 'resqr-token resqr-refresh-token');
        res.set('resqr-token', newTokens.token);
        res.set('resqr-refresh-token', newTokens.refreshToken);
      }
      req.restaurant = newTokens.restaurant;
    }
  }
  next();
};

app.use(addRestaurant);

app.use(
  graphqlEndpoint,
  bodyParser.json(),
  apolloUploadExpress(),
  graphqlExpress(req => ({
    schema,
    context: {
      models,
      restaurant: req.restaurant, // this restaurant is provided by the middleware above
      SECRET,
      SECRET2,
    },
  })),
);

app.use('/files', express.static('files'));

app.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: graphqlEndpoint,
    subscriptionsEndpoint: 'ws://resqr.herokuapp.com/subscriptions',
  }),
);

const server = createServer(app);

const PORT = process.env.PORT || 3000;

models.sequelize.sync().then(() => {
  server.listen(PORT, () => {
    console.log(`listening port ${process.env.PORT}`);
    // eslint-disable-next-line
		new SubscriptionServer(
      {
        execute,
        subscribe,
        schema,
        // token and refreshToken are being passed from where the subscription link
        // is being created on the client
        // whatever gets returned from this hook or lifecycle event methods are context
        // for subscription
        onConnect: async ({ token, refreshToken }, webSocket) => {
          if (token && refreshToken) {
            try {
              const { user } = jwt.verify(token, SECRET);
              return { models, user };
              // user = { id: 8 }
            } catch (e) {
              const newTokens = await refreshTokens(token, refreshToken, models, SECRET, SECRET2);
              return { user: newTokens.user, models };
            }
          }

          return { models };
        },
      },
      {
        server,
        path: '/subscriptions',
      },
    );
  });
});
