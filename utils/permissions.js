const createResolver = (resolver) => {
  const baseResolver = resolver;
  baseResolver.createResolver = (childResolver) => {
    const newResolver = async (parent, args, context, info) => {
      await resolver(parent, args, context, info);
      return childResolver(parent, args, context, info);
    };
    return createResolver(newResolver);
  };
  return baseResolver;
};

export const requireAuth = createResolver((parent, args, { restaurant }) => {
  if (!restaurant || !restaurant.id) {
    throw new Error('Not Authenticated');
  }
});

export const idk = () => {};
