import jwt from 'jsonwebtoken';

import createTokens from './createTokens';

export default async (token, refreshToken, models, SECRET, SECRET2) => {
  let restaurantId = 0;
  try {
    const {
      restaurant: { id },
    } = jwt.decode(refreshToken);
    restaurantId = id;
  } catch (err) {
    return {};
  }

  if (!restaurantId) {
    return {};
  }

  const restaurant = await models.Restaurant.findOne({ where: { id: restaurantId }, raw: true });
  if (!restaurant) {
    return {};
  }

  const refreshSecret = restaurant.password + SECRET2;

  try {
    jwt.verify(refreshToken, refreshSecret);
  } catch (err) {
    return {};
  }

  const [newToken, newRefreshToken] = await createTokens(restaurant, SECRET, refreshSecret);
  return {
    token: newToken,
    refreshToken: newRefreshToken,
    restaurant,
  };
};
