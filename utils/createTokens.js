import jwt from 'jsonwebtoken';
import pick from 'lodash/pick';

export default async (restaurant, secret, secret2) => {
  const createToken = jwt.sign({ restaurant: pick(restaurant, ['id', 'name']) }, secret, {
    expiresIn: '1h',
  });

  const createRefrestToken = jwt.sign({ restaurant: pick(restaurant, 'id') }, secret2, {
    expiresIn: '7d',
  });

  return [createToken, createRefrestToken];
};
