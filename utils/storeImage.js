import moment from 'moment';
import mkdirp from 'mkdirp';
import fs from 'fs';

const uploadDir = './files';
mkdirp.sync(uploadDir);

const storeFS = ({ stream, filename }) => {
  const url = `${uploadDir}/${moment()}-${filename}`;
  return new Promise((resolve, reject) =>
    stream
      .on('error', (error) => {
        if (stream.truncated) {
          // Delete the truncated file
          fs.unlinkSync(url);
        }
        reject(error);
      })
      .on('end', () => resolve({ url }))
      .pipe(fs.createWriteStream(url)));
};

// export default upload =>
//   new Promise(async (resolve) => {
//     const { stream, filename, mimetype } = await upload;
//     if (!mimetype.startsWith('image')) {
//       return resolve({ mimetype, url: null });
//     }
//     const { url } = await storeFS({ stream, filename });
//     resolve({ mimetype, url });
//   });

export default async (upload) => {
  const { stream, filename, mimetype } = await upload;
  if (!mimetype.startsWith('image')) {
    return { mimetype, url: null };
  }
  const { url } = await storeFS({ stream, filename });
  return { mimetype, url };
};
