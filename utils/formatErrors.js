import pick from 'lodash/pick';
/* formatErrors is used to extract the the path and message out of the error message
 * and sent as error type
*/
export default (err, models) => {
  if (err instanceof models.sequelize.ValidationError) {
    return err.errors.map(x => pick(x, ['path', 'message']));
  }
  return [{ path: 'unknown', message: 'something went wrong' }];
};
