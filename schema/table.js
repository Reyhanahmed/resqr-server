export default `
	type Table {
		id: Int!
		name: String!
	}

	type TableResponse {
		ok: Boolean!
		table: Table
		errors: [Error!]
	}

	type Query {
		allTables: [Table!]
	}

	type Mutation {
		createTable(name: String!): TableResponse!
		deleteTable(id: Int!): Boolean!
		updateTable(name: String!, id: Int!): TableResponse!
	}
`;
