export default `

	type OrderResponse {
		ok: Boolean!
		errors: [Error!]
	}

	type OrderMenu {
		quantity: Int!
		mealId: Int!
		orderId: Int!
		varId: Int!
		variety: Variety!
	}

	type MealObj {
		id: Int!
		name: String!
		description: String!
		order_menu: OrderMenu!
	}

	type Order {
		id: Int!
		status: String!
		totalPrice: Int!
		note: String!
		tableId: Int!
		meals: [MealObj!]
	}

	type OrderObj {
		tableName: String!
		tableId: Int!
		orders: [Order!]
	}

	type GetOrderResponse {
		ok: Boolean!
		errors: [Error!]
		ordersArr: [OrderObj!]
	}

	type UpdatedOrder {
		id: Int!
		totalPrice: Int!
		tableId: Int!
		status: String!
		note: String!
	}

	type Subscription {
		newOrder: OrderObj!
	}

	type Query {
		getOrders: GetOrderResponse!
	}

	type Mutation {
		createOrder(note: String, totalPrice: Int!, meals: [Int!], quantities: [Int!], tableId: Int!, varieties: [Int!] ): OrderResponse!
		updateOrderStatus(orderId: Int!, status: String!):  UpdatedOrder!
	}
`;
