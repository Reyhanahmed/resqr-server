export default `
	type Variety {
		id: Int!
		name: String!
		price: Int!
		mealId: Int
	}

	type Query {
		getVarieties(mealId: Int!): [Variety!]
	}
`;
