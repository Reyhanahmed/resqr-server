export default `
	type Category {
		id: Int!
		name: String!
	}

	type CategoryResponse {
		ok: Boolean!
		category: Category
		errors: [Error!]
	}

	type Query {
		allCategories: [Category!]
	}

	type Mutation {
		createCategory(name: String!): CategoryResponse!
		deleteCategory(id: Int!): Boolean!
		updateCategory(name: String!, id: Int!): CategoryResponse!
	}
`;
