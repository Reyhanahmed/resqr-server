export default `
type Meal {
	id: Int!
	name: String!
	description: String!
	image: String!
	category: Category
	varieties: [Variety!]
}

type MealResponse {
	ok: Boolean!
	meal: Meal
	errors: [Error!]
}

type Query {
	allMeals: [Meal!]
	getRestaurantMeals(catId: Int!): [Meal!]
	getMeal(mealId: Int!): MealResponse!
}

type Mutation {
	createMeal(name: String!, description: String!, image: Upload!, category: Int! ,prices: [Int!], varieties: [String!]): MealResponse!
	updateMeal(name: String!, description: String!, image: Upload, category: Int! ,prices: [Int!], varieties: [String!], varIds: [Int!], mealId: Int!): MealResponse!
	deleteMeal(mealId: Int!, varIds: [Int!]): MealResponse!
}
`;
