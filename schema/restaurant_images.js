export default `
	
	scalar Upload

	type Image {
		id: Int!
		url: String!
		type: String!
	}

	type ResImageResponse {
		ok: Boolean!
		images: [Image!]
		errors: [Error!]
	}

	type Mutation {
		uploadResImage(file: Upload!): Image!
		deleteResImage(id: Int!, url: String!): Boolean!
	}

	type Query {
		getResImages: ResImageResponse!
	}
`;
