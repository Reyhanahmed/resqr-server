export default `
	type Restaurant {
		id: Int!
		name: String
		email: String!
		address: String
		restaurantImages: [Image!]
		categories: [Category!]
	}

	type AuthResponse {
		ok: Boolean!
		restaurant: Restaurant
		errors: [Error!]
		token: String
		refreshToken: String
	}

	type RestaurantResponse {
		ok: Boolean!
		restaurant: Restaurant
		errors: [Error!]
	}

	type Query {
		getRestaurant(tableId: Int!): RestaurantResponse!
		me: Restaurant!
	}

	type Mutation {
		registerRestaurant(email: String!, password: String!): AuthResponse!
		loginRestaurant(email: String!, password: String!): AuthResponse!
		updateResInfo(name: String!, address: String!): RestaurantResponse!
	}
`;
