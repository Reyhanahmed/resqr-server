export default (Sequelize, DataTypes) => {
  const Order = Sequelize.define('order', {
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Waiting',
    },
    totalPrice: {
      type: DataTypes.STRING,
      validate: {
        isNumeric: {
          args: true,
          msg: 'Total price of order can only contain numbers.',
        },
      },
    },
    note: DataTypes.STRING,
  });

  Order.associate = (models) => {
    Order.belongsTo(models.Table, {
      foreignKey: {
        name: 'tableId',
        field: 'table_id',
      },
    });

    Order.belongsToMany(models.Meal, {
      through: models.Order_Menu,
      foreignKey: {
        name: 'orderId',
        field: 'order_id',
      },
    });
  };
  return Order;
};
