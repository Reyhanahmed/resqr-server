export default (Sequelize, DataTypes) => {
  const Meal = Sequelize.define('meal', {
    name: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [3, 50],
          msg: 'The meal name needs to be between 5 and 15 characters long',
        },
        notEmpty: {
          args: true,
          msg: 'Cannot leave meal name empty',
        },
      },
    },
    description: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [10, 250],
          msg: 'The description needs to be between 10 and 150 characters long',
        },
      },
    },
    image: DataTypes.STRING,
  });

  Meal.associate = (models) => {
    Meal.hasMany(models.Variety, {
      foreignKey: {
        name: 'mealId',
        field: 'meal_id',
      },
    });

    Meal.belongsToMany(models.Order, {
      through: models.Order_Menu,
      foreignKey: {
        name: 'mealId',
        field: 'meal_id',
      },
    });

    Meal.belongsTo(models.Category, {
      foreignKey: {
        name: 'catId',
        field: 'cat_id',
      },
    });

    Meal.belongsToMany(models.Variety, {
      through: models.Order_Menu,
      foreignKey: {
        name: 'mealId',
        field: 'meal_id',
      },
    });
  };
  return Meal;
};
