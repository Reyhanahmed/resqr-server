export default (Sequelize, DataTypes) => {
  const OrderMenu = Sequelize.define('order_menu', {
    quantity: DataTypes.INTEGER,
  });

  OrderMenu.associate = (models) => {
    OrderMenu.belongsTo(models.Variety, {
      foreignKey: {
        name: 'varId',
        field: 'var_id',
      },
    });
  };
  return OrderMenu;
};
