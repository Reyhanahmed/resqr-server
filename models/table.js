export default (Sequelize, DataTypes) => {
  const Table = Sequelize.define('table', {
    status: {
      type: DataTypes.STRING,
      defaultValue: 'open',
    },
    name: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        message: 'Tables cannot have same name',
      },
      validate: {
        len: {
          args: [3, 50],
          msg: 'The name of the table needs to be between 3 and 10 characters long',
        },
      },
    },
  });

  Table.associate = (models) => {
    Table.belongsTo(models.Restaurant, {
      foreignKey: {
        name: 'resId',
        field: 'res_id',
      },
    });

    Table.hasMany(models.Order, {
      foreignKey: {
        name: 'tableId',
        field: 'table_id',
      },
    });
  };
  return Table;
};
