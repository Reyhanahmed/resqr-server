export default (Sequelize, DataTypes) => {
  const Category = Sequelize.define('category', {
    name: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [3, 30],
          msg: 'The category name needs to be between 3 and 15 characters long',
        },
        notEmpty: {
          args: true,
          msg: 'Cannot leave category name empty',
        },
      },
    },
  });

  Category.associate = (models) => {
    Category.belongsTo(models.Restaurant, {
      foreignKey: 'resId',
      field: 'res_id',
    });

    Category.hasMany(models.Meal, {
      foreignKey: 'catId',
      field: 'cat_id',
    });
  };

  return Category;
};
