export default (Sequelize, DataTypes) => {
  const Variety = Sequelize.define('variety', {
    name: {
      type: DataTypes.STRING,
      validate: {
        isAlphanumeric: {
          args: true,
          msg: 'The variety name can only contain letters and numbers',
        },
        len: {
          args: [5, 20],
          msg: 'The variety name needs to be between 5 and 10 characters long',
        },
      },
    },
    price: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: 'Cannot leave meal price empty',
        },
        isNumeric: {
          args: true,
          msg: 'The price can only contain numbers',
        },
      },
    },
  });

  Variety.associate = (models) => {
    Variety.belongsTo(models.Meal, {
      foreignKey: {
        name: 'mealId',
        field: 'meal_id',
      },
    });

    Variety.hasMany(models.Order_Menu, {
      foreignKey: {
        name: 'varId',
        field: 'var_id',
      },
    });
  };
  return Variety;
};
