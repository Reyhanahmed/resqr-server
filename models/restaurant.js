import bcrypt from 'bcryptjs';

export default (Sequelize, DataTypes) => {
  const Restaurant = Sequelize.define(
    'restaurant',
    {
      name: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [3, 40],
            msg: 'The restaurant name needs to be between 5 and 25 characters long',
          },
        },
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'Invalid email',
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [5, 100],
            msg: 'The password needs to be between 5 and 100 characters long',
          },
        },
      },
      address: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [10, 120],
            msg: 'The address needs be between 10 and 120 characters long',
          },
        },
      },
    },
    {
      hooks: {
        // this hook hashes the password if it passes the validation tests
        afterValidate: async (restaurant) => {
          /* eslint-disable */
					if (restaurant.password) {
						restaurant.password = await bcrypt.hashSync(restaurant.password, 12);
					}
					/* eslint-enable */
        },
      },
    },
  );

  Restaurant.associate = (models) => {
    Restaurant.hasMany(models.Table, {
      foreignKey: {
        name: 'resId',
        field: 'res_id',
      },
    });

    Restaurant.hasMany(models.Restaurant_Images, {
      foreignKey: {
        name: 'resId',
        field: 'res_id',
      },
    });

    Restaurant.hasMany(models.Category, {
      foreignKey: {
        name: 'resId',
        field: 'res_id',
      },
    });
  };
  return Restaurant;
};
