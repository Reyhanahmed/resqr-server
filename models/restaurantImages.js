export default (Sequelize, DataTypes) => {
  const RestaurantImages = Sequelize.define('restaurant_images', {
    url: DataTypes.STRING,
    type: DataTypes.STRING,
  });

  RestaurantImages.associate = (models) => {
    RestaurantImages.belongsTo(models.Restaurant, {
      foreignKey: {
        name: 'resId',
        field: 'res_id',
      },
    });
  };
  return RestaurantImages;
};
