// to export a postgres database file use; pg_dump -U USERNAME DBNAME > dbexport.pgsql

// to import a postgres database file use; psql -U USERNAME DBNAME < dbexport.pgsql

// NOTE: BelongsTo will add the foreignKey on the source where hasOne will add on the target

import Sequelize from 'sequelize';
import dotenv from 'dotenv';

dotenv.config();

let sequelize;
if (process.env.DATABASE_URL) {
  sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialect: 'postgres',
    // This is to remove the warning we get when starting the server
    operatorsAliases: Sequelize.OP,
    define: {
      // this property makes all the auto generated fields of the DB snake_case
      // e.g. createdAt becomes created_at
      underscored: true,
    },
  });
} else {
  sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    dialect: 'postgres',
    // This is to remove the warning we get when starting the server
    operatorsAliases: Sequelize.OP,
    define: {
      // this property makes all the auto generated fields of the DB snake_case
      // e.g. createdAt becomes created_at
      underscored: true,
    },
  });
}

const models = {
  Restaurant: sequelize.import('./restaurant.js'),
  Meal: sequelize.import('./meal.js'),
  Order: sequelize.import('./order.js'),
  Table: sequelize.import('./table.js'),
  Order_Menu: sequelize.import('./orderMenu.js'),
  Variety: sequelize.import('./variety.js'),
  Restaurant_Images: sequelize.import('./restaurantImages.js'),
  Category: sequelize.import('./category.js'),
};

// this checks if all the properties (i.e. tables) in the models (i.e. database)
// has an 'associate' function and if yes then call it with the models arg.
Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;
