import bcrypt from 'bcryptjs';

import createTokens from '../utils/createTokens';
import formatErrors from '../utils/formatErrors';
import { requireAuth } from '../utils/permissions';

export default {
  Query: {
    getRestaurant: async (parent, { tableId }, { models }) => {
      try {
        const table = await models.Table.findOne({ where: { id: tableId } });
        const res = await models.Restaurant.findOne({
          where: { id: table.resId },
          include: [
            { model: models.Restaurant_Images },
            { model: models.Category, where: { resId: table.resId } },
            // { model: models.Table, where: { resId: table.resId } },
          ],
        });
        const { restaurant_images, categories, ...rest } = res;

        return {
          ok: true,
          restaurant: { restaurantImages: restaurant_images, categories, ...rest.dataValues },
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    },
    me: requireAuth.createResolver(async (parent, args, { models, restaurant }) =>
      models.Restaurant.findOne({
        where: { id: restaurant.id },
        raw: true,
      })),
  },
  Mutation: {
    updateResInfo: requireAuth.createResolver(async (parent, { name, address }, { models, restaurant }) => {
      try {
        const update = await models.Restaurant.update(
          { name, address },
          { where: { id: restaurant.id }, returning: true },
        );
        return {
          ok: true,
          restaurant: update[1][0].dataValues,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    registerRestaurant: async (parent, args, { models, SECRET, SECRET2 }) => {
      try {
        const res = await models.Restaurant.create(args);
        const refreshTokenSecret = res.password + SECRET2;
        const [token, refreshToken] = await createTokens(res, SECRET, refreshTokenSecret);
        return {
          ok: true,
          restaurant: res,
          token,
          refreshToken,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    },
    loginRestaurant: async (parent, { email, password }, { models, SECRET, SECRET2 }) => {
      try {
        const res = await models.Restaurant.findOne({ where: { email } }, { raw: true });
        if (!res) {
          return {
            ok: false,
            errors: [{ path: 'email', message: 'Wrong Email!' }],
          };
        }
        const valid = await bcrypt.compareSync(password, res.password);
        if (!valid) {
          return {
            ok: false,
            errors: [{ path: 'password', message: 'Wrong Password!' }],
          };
        }
        const refreshTokenSecret = res.password + SECRET2;
        const [token, refreshToken] = await createTokens(res, SECRET, refreshTokenSecret);
        return {
          ok: true,
          token,
          refreshToken,
          restaurant: res,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    },
  },
};
