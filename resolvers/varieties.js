export default {
  Query: {
    getVarieties: (parent, { mealId }, { models }) =>
      models.Variety.findAll({ where: { mealId }, raw: true }),
  },
};
