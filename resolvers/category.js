import { requireAuth } from '../utils/permissions';
import formatErrors from '../utils/formatErrors';

export default {
  Mutation: {
    createCategory: requireAuth.createResolver(async (parent, { name }, { models, restaurant }) => {
      try {
        const category = await models.Category.create({ name, resId: restaurant.id });
        return {
          ok: true,
          category,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    deleteCategory: requireAuth.createResolver(async (parent, { id }, { models }) => {
      await models.Category.destroy({ where: { id } });
      return true;
    }),
    updateCategory: requireAuth.createResolver(async (parent, { name, id }, { models }) => {
      try {
        const res = await models.Category.update({ name }, { where: { id }, returning: true });
        return {
          ok: true,
          category: res[1][0].dataValues,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
  Query: {
    allCategories: async (parent, args, { models, restaurant }) =>
      models.Category.findAll({ where: { resId: restaurant.id } }),
  },
};
