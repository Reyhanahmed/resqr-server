import fs from 'fs';
import { GraphQLUpload } from 'apollo-upload-server';
import dotenv from 'dotenv';

import { requireAuth } from '../utils/permissions';
import formatErrors from '../utils/formatErrors';
import storeImage from '../utils/storeImage';

dotenv.config();

export default {
  Upload: GraphQLUpload,
  Image: {
    url: parent => (parent.url ? `${process.env.URL}${parent.url}` : parent.url),
  },
  Mutation: {
    uploadResImage: requireAuth.createResolver(async (parent, { file }, { models, restaurant }) => {
      try {
        if (file) {
          const { mimetype: type, url } = await storeImage(file);
          const res = await models.Restaurant_Images.create({
            type,
            url,
            resId: restaurant.id,
          });
          return res;
        }
      } catch (err) {
        console.log(err.message);
        return false;
      }
    }),
    deleteResImage: requireAuth.createResolver(async (parent, { id, url }, { models }) => {
      await models.Restaurant_Images.destroy({ where: { id } });
      const filepaths = url.split('./');
      fs.unlinkSync(filepaths[1]);
      return true;
    }),
  },
  Query: {
    getResImages: requireAuth.createResolver(async (parent, args, { models, restaurant }) => {
      try {
        const res = await models.Restaurant_Images.findAll({ where: { resId: restaurant.id } });
        return {
          ok: true,
          images: res,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
};
