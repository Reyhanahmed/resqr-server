import dotenv from 'dotenv';
import { requireAuth } from '../utils/permissions';
import formatErrors from '../utils/formatErrors';
import storeImage from '../utils/storeImage';

dotenv.config();

export default {
  Meal: {
    image: parent => (parent.image ? `${process.env.URL}${parent.image}` : parent.image),
  },
  Query: {
    allMeals: requireAuth.createResolver(async (parent, args, { models, restaurant }) => {
      const meals = await models.Meal.findAll({
        include: [
          { model: models.Variety },
          { model: models.Category, where: { resId: restaurant.id } },
        ],
      });

      return meals;
    }),
    getRestaurantMeals: (parent, { catId }, { models }) =>
      models.Meal.findAll({ where: { catId } }),
    getMeal: requireAuth.createResolver(async (parent, { mealId }, { models, restaurant }) => {
      try {
        const meal = await models.Meal.findOne({
          where: { id: mealId },
          include: [
            { model: models.Variety },
            { model: models.Category, where: { resId: restaurant.id } },
          ],
        });
        return {
          ok: true,
          meal,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
  Mutation: {
    createMeal: requireAuth.createResolver(async (parent, {
      name, description, image, prices, varieties, category,
    }, { models }) => {
      try {
        const { url } = await storeImage(image);
        const response = await models.sequelize.transaction(async (transaction) => {
          const meal = await models.Meal.create(
            {
              name,
              description,
              image: url,
              catId: category,
            },
            { transaction },
          );
          const bulkVarieties = varieties.map((variety, i) => ({
            name: variety,
            price: prices[i],
            mealId: meal.id,
          }));
          await models.Variety.bulkCreate(bulkVarieties, { transaction });
          const cat = await models.Category.findOne({ where: { id: category } });
          meal.category = cat;
          return meal;
        });
        const varietiess = await models.Variety.findAll({ where: { mealId: response.id } });
        response.varieties = varietiess;
        return {
          ok: true,
          meal: response,
          errors: null,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    updateMeal: requireAuth.createResolver(async (
      parent,
      {
        name, description, image, prices, varieties, category, varIds, mealId,
      },
      { models },
    ) => {
      try {
        let url;
        if (image) {
          ({ url } = await storeImage(image));
        }
        if (url) {
          await models.Meal.update(
            {
              name,
              description,
              image: url,
              catId: category,
            },
            { where: { id: mealId }, returning: true },
          );
        } else {
          await models.Meal.update(
            {
              name,
              description,
              catId: category,
            },
            { where: { id: mealId }, returning: true },
          );
        }
        const varPromises = [];
        varIds.every((id, k) => {
          varPromises.push(models.Variety.update(
            {
              name: varieties.shift(),
              price: prices.shift(),
            },
            { where: { id } },
          ));
          // eslint-disable-next-line
						delete varIds[k];
          return varieties.length >= 1;
        });
        const varietiesToDelete = varIds.filter(id => id !== undefined);
        if (varietiesToDelete.length > 1) {
          varietiesToDelete.forEach((id) => {
            varPromises.push(models.Variety.destroy({ where: { id } }));
          });
        } else if (varietiesToDelete.length === 1) {
          varPromises.push(models.Variety.destroy({ where: { id: varietiesToDelete[0] } }));
        }
        await Promise.all(varPromises);
        if (varieties.length > 1) {
          const bulkVarieties = varieties.map((variety, i) => ({
            name: variety,
            price: prices[i],
            mealId,
          }));
          await models.Variety.bulkCreate(bulkVarieties);
        } else if (varieties.length === 1) {
          await models.Variety.create({
            name: varieties[0],
            price: prices[0],
            mealId,
          });
        }
        return {
          ok: true,
          errors: null,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    deleteMeal: requireAuth.createResolver(async (parent, { mealId, varIds }, { models }) => {
      try {
        await models.sequelize.transaction(async (transaction) => {
          const mealDestroyPromise = models.Meal.destroy(
            { where: { id: mealId } },
            { transaction },
          );
          const varietiesDestroyPromise = models.Variety.destroy(
            { where: { id: varIds } },
            { transaction },
          );
          await Promise.all([mealDestroyPromise, varietiesDestroyPromise]);
        });
        return {
          ok: true,
          errors: null,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
};
