import { PubSub } from 'graphql-subscriptions';
import formatErrors from '../utils/formatErrors';
import { requireAuth } from '../utils/permissions';

const NEW_ORDER = 'NEW_ORDER';
const pubsub = new PubSub();

export default {
  Subscription: {
    newOrder: {
      subscribe: () => pubsub.asyncIterator(NEW_ORDER),
    },
  },
  Query: {
    getOrders: requireAuth.createResolver(async (parent, args, { models, restaurant }) => {
      try {
        const response = await models.Table.findAll({
          where: {
            resId: restaurant.id,
          },
          include: [
            {
              model: models.Order,
              include: [{ model: models.Meal }],
            },
          ],
        });
        const orderIds = [];
        response.forEach((table) => {
          table.dataValues.orders.forEach((order) => {
            orderIds.push(order.id);
          });
        });
        const results = [];
        response.forEach((res, i) => {
          const obj = {};
          obj.tableName = res.dataValues.name;
          obj.tableId = res.dataValues.id;
          obj.orders = res.dataValues.orders.map((ord) => {
            if (ord.dataValues.tableId === res.id) {
              const ordersObj = { ...ord.dataValues };
              ordersObj.meals = ordersObj.meals.map(meal => meal.dataValues);
              return ordersObj;
            }
          });
          results.push(obj);
        });
        const varIds = [];
        results.forEach((res) => {
          res.orders.forEach((order) => {
            order.meals.forEach(meal => varIds.push(meal.order_menu.dataValues.varId));
          });
        });
        const newVarIds = [...new Set(varIds)];
        const varResponse = await models.Variety.findAll({
          where: { id: { [models.Sequelize.Op.in]: newVarIds } },
        });

        results.forEach((res) => {
          res.orders.forEach((order) => {
            order.meals.forEach((meal) => {
              meal.order_menu = meal.order_menu.dataValues;
              varResponse.forEach((varRes) => {
                if (meal.order_menu.varId === varRes.dataValues.id) {
                  meal.order_menu.variety = varRes.dataValues;
                }
              });
            });
          });
        });
        return {
          ok: true,
          ordersArr: results,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
  Mutation: {
    createOrder: async (
      parent,
      {
        meals, note, totalPrice, tableId, quantities, varieties,
      },
      { models },
    ) => {
      try {
        const response = await models.sequelize.transaction(async (transaction) => {
          const order = await models.Order.create(
            {
              totalPrice,
              tableId,
              note,
            },
            { transaction, returning: true },
          );
          const bulkMeals = meals.map((mealId, i) => ({
            mealId,
            orderId: order.id,
            quantity: quantities[i],
            varId: varieties[i],
          }));
          const orderMenu = await models.Order_Menu.bulkCreate(bulkMeals, { transaction });

          const asyncFunc = async () => {
            const mealsPromise = models.Meal.findAll({
              where: { id: { [models.Sequelize.Op.in]: meals } },
            });
            const varietyPromise = models.Variety.findAll({
              where: { id: { [models.Sequelize.Op.in]: varieties } },
            });

            const tablePromise = models.Table.find({
              where: { id: tableId },
              raw: true,
            });

            const [mealsResponse, varietyResponse, tableResponse] = await Promise.all([
              mealsPromise,
              varietyPromise,
              tablePromise,
            ]);

            const resultObj = {};
            resultObj.tableId = tableResponse.id;
            resultObj.tableName = tableResponse.name;

            resultObj.orders = [order.dataValues];
            resultObj.orders[0].meals = orderMenu.map((orM) => {
              let mealObj;
              mealsResponse.forEach((mealRes) => {
                if (mealRes.dataValues.id === orM.dataValues.mealId) {
                  mealObj = { ...mealRes.dataValues, order_menu: { ...orM.dataValues } };
                }
              });
              varietyResponse.forEach((varRes) => {
                if (varRes.dataValues.id === orM.dataValues.varId) {
                  mealObj.order_menu = {
                    ...mealObj.order_menu,
                    variety: { ...varRes.dataValues },
                  };
                }
              });
              return mealObj;
            });
            pubsub.publish(NEW_ORDER, {
              newOrder: resultObj,
            });
          };

          asyncFunc();
          return {
            ok: true,
          };
        });
        return response;
      } catch (err) {
        console.log('errrrrr', err);
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    },
    updateOrderStatus: async (parent, { orderId, status }, { models }) => {
      let newStatus;
      if (status === 'Waiting') {
        newStatus = 'In Progress';
      } else if (status === 'In Progress') {
        newStatus = 'Completed';
      } else if (status === 'Completed') {
        newStatus = 'Finished';
      }
      const result = await models.Order.update(
        { status: newStatus },
        { where: { id: orderId }, returning: true },
      );
      console.log(result[1][0].dataValues);
      return result[1][0].dataValues;
    },
  },
};
