import { requireAuth } from '../utils/permissions';
import formatErrors from '../utils/formatErrors';

export default {
  Mutation: {
    createTable: requireAuth.createResolver(async (parent, { name }, { models, restaurant }) => {
      try {
        const table = await models.Table.create({ name, resId: restaurant.id });
        return {
          ok: true,
          table,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
    deleteTable: requireAuth.createResolver(async (parent, { id }, { models }) => {
      await models.Table.destroy({ where: { id } });
      return true;
    }),
    updateTable: requireAuth.createResolver(async (parent, { name, id }, { models }) => {
      try {
        const res = await models.Table.update({ name }, { where: { id }, returning: true });
        return {
          ok: true,
          table: res[1][0].dataValues,
        };
      } catch (err) {
        return {
          ok: false,
          errors: formatErrors(err, models),
        };
      }
    }),
  },
  Query: {
    allTables: async (parent, args, { models, restaurant }) =>
      models.Table.findAll({ where: { resId: restaurant.id } }),
  },
};
